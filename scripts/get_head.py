"""
This file uses snakemake to read from a csv and save just the head to the output.

"""
import pandas as pd

# this ensures the script works without snakemake
try:
    in_csv_path = snakemake.input[0]
    out_csv_path = snakemake.output[0]
except NameError:
    in_csv_path = 'in_data/test_in.csv'
    out_csv_path = 'processed_data/final-a1.csv'

print("in is %s" % in_csv_path)
print("out is %s" % out_csv_path)

df = pd.read_csv(in_csv_path)
df.head().to_csv(out_csv_path)
