# README #

### What is this repository for? ###

This is a template designed to allow you to quickly setup a snakemake flow.

### How do I get set up? ###

'''pip install -r requirements.txt'''


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Louis Schamroth-Green
louis.schamroth-green@arup.com