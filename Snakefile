"""
this is the snakemake file. 
It loads in the workflow_data.csv stored in workflow.
"""
import pandas as pd
import platform

df = pd.read_csv("workflow/workflow_data.csv")

df_paths = pd.read_csv("workflow/run_paths.csv")
series_paths = df_paths[df_paths['pc_name'] == platform.node()]
python_path = series_paths['Python']
print("python path is %s" % python_path)

rule final:
    input:
        expand("processed_data/final-{file}.csv", file=df['assessment'].tolist())

rule processes_data:
    input:
        'in_data/test_in.csv'
    output:
        "processed_data/final-{file}.csv"
    script:
        'scripts/get_head.py'

rule download_data:
    output:
        'in_data/test_in.csv'
    shell:
        "wget -O in_data/test_in.csv http://samplecsvs.s3.amazonaws.com/SacramentocrimeJanuary2006.csv"

    
